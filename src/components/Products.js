import Productdata from '../productdata.json'
import Product from './Product'

const Products = ({ecom, onAddToCart}) => {
    return (
        <section id="content">
            <div className="container">
                <div className="row no-gutters">
                    {Productdata.map((product) => (
                        <div className="col-4 product-item" key={product.id}><Product product={product} ecom={ecom} onAddToCart={onAddToCart}/></div>
                    ))}
                </div>
            </div>
        </section>
    )
}

export default Products
