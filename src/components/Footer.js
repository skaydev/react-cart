const Footer = () => {
    let d = new Date();
    let year = d.getFullYear();

    return (
        <footer>
            <div className="container">
                <p>Copyright &copy; {year}</p>
            </div>
        </footer>
    )
}

export default Footer
