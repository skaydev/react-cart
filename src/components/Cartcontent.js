import trash from '../images/trash.png'

const Cartcontent = ({cart, ecom, onDelCartitem}) => {

    const calculatedTax = (price, count) => {
        return ((price * count * ecom.tax)/100).toFixed(2)
    }

    const calculatedItemSubTotal = (price, count) => {
        return (price + ((price * count * ecom.tax)/100)).toFixed(2)
    }

    const calculatedSubTotal = (cart) => {
        let arrItemSubTotal = [];

        cart.forEach(cartItem => {
            arrItemSubTotal.push(calculatedItemSubTotal(cartItem.price, cartItem.count))
        });

        let subtotal = arrItemSubTotal.reduce((total, val) => {
            return total += Number(val);
        }, 0)

        return subtotal
    }

    const calculatedTotalTax = (cart) => {
        let arrItemTax = [];

        cart.forEach(cartItem => {
            arrItemTax.push(calculatedTax(cartItem.price, cartItem.count))
        });

        let totalTax = arrItemTax.reduce((total, val) => {
            return total += Number(val);
        }, 0)

        return totalTax
    }

    const calculatedTotal = (cart) => {
        return calculatedSubTotal(cart) + ecom.shipping
    }

    return (
        <div className="row no-gutters">
            <div className="col-9">
                <div className="grid-wrap">
                    <div className="grid-header">
                        <div>Product</div>
                        <div>Price</div>
                        <div className="qty">Qty</div>
                        <div>Tax</div>
                        <div>Subtotal</div>
                        <div className="actions"></div>
                    </div>
                    {cart.map((cartItem, idx) => (
                        <div key={idx} className="grid-item">
                            <div>{cartItem.name}</div>
                            <div>{ecom.currency} {cartItem.price.toFixed(2)}</div>
                            <div className="qty">{cartItem.count}</div>
                            <div>{ecom.currency} {calculatedTax(cartItem.price, cartItem.count)}</div>
                            <div>{ecom.currency} {calculatedItemSubTotal(cartItem.price, cartItem.count)}</div>
                            <div className="actions"><img src={trash} alt="Trash icon" onClick={() => onDelCartitem(cartItem.id)}/></div>
                        </div>
                    ))}
                </div>
            </div>
            <div className="col-3">
                <div className="total-wrap">
                    <div className="total-inner">
                        <div className="record">
                            <div className="label">Subtotal</div>
                            <div>{ecom.currency} {calculatedSubTotal(cart).toFixed(2)}</div>
                        </div>
                        <div className="record">
                            <div className="label">Shipping</div>
                            <div>{ecom.currency} {ecom.shipping.toFixed(2)}</div>
                        </div>
                        <div className="record">
                            <div className="label">Tax</div>
                            <div>{ecom.currency} {calculatedTotalTax(cart).toFixed(2)}</div>
                        </div>
                        <div className="record">
                            <div className="label">Total</div>
                            <div>{ecom.currency} {calculatedTotal(cart).toFixed(2)}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Cartcontent
