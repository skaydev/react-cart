import Cartcontent from './Cartcontent'

const Cart = ({cart, ecom, onDelCartitem}) => {
    return (
        <section id="content">
            <div className="container">
                {cart.length > 0 ? <Cartcontent cart={cart} ecom={ecom} onDelCartitem={onDelCartitem}/>: <p className="no-items">No items in your cart</p>}
            </div>
        </section>
    )
}

export default Cart
