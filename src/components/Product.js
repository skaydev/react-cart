import placeholder from '../images/placeholder.png'
import Button from './Button'

const Product = ({product, ecom, onAddToCart}) => {
    return (
        <div className="product-wrap">
            <img src={placeholder} alt="Placeholder" className="placeholder"/>
            <div className="name">{product.name}</div>
            <div className="price">{ecom.currency} {product.price.toFixed(2)}</div>
            <Button text="Add to cart" onClick={() => onAddToCart(product)}/>
        </div>
    )
}

export default Product
