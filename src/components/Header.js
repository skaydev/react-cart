import {Link} from 'react-router-dom'

const Header = ({cart}) => {

    const calculatedCartTotal = (cart) => {
        let cartTotal = ""
        if(cart.length > 0) {
            let arrItemCount = []

            cart.forEach(cartItem => {
                arrItemCount.push(cartItem.count)
            });
    
            let totalCount = arrItemCount.reduce((total, val) => {
                return total += Number(val);
            }, 0)

            cartTotal = "("+totalCount+")"
        } 
        return cartTotal
    }

    return (
        <header id='header'>
            <div className='container'>
                <div className="row no-gutters">
                    <div id="logo" className='col-9'>
                        Shoe store
                    </div>
                    <div id="navigation" className='col-3'>
                        <nav>
                            <Link to='/'>Shop</Link>
                            <Link to='/cart'>Cart {calculatedCartTotal(cart)}</Link>
                        </nav>
                    </div>
                </div>
            </div>
        </header> 
    )
}

export default Header
