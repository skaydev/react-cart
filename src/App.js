import {useState, useEffect} from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Header from './components/Header'
import Footer from './components/Footer'
import Products from './components/Products'
import Cart from './components/Cart'

const cartInStorage = JSON.parse(localStorage.getItem('cart') || '[]');

function App() {
  const ecom = {
    currency: 'Rs',
    tax: 1.23,
    shipping: 500
  }

  const [cart, setCart] = useState(cartInStorage)

  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cart))
  }, [cart]);

  const addToCart = (product) => {
    const cartHasTheProduct = cart.some(cartItems => cartItems.id === product.id)
    if(cartHasTheProduct) {
      const cartIndex = cart.findIndex(cartItems => cartItems.id === product.id)
      cart[cartIndex].count += 1
      setCart([...cart])
    } else {
      product.count = 1
      setCart([...cart,product])
    }
  }

  const delCartItem = (id) => {       
    setCart(cart.filter((cartItem) => cartItem.id !== id))
  }

  return (
    <div>
      <Router>
        <Header cart={cart}/>
        <Route path='/' exact render={(props) => (
            <>
                <Products ecom={ecom} onAddToCart={addToCart}/>
            </>
        )} />
        <Route path='/cart' exact render={(props) => (
            <>
                <Cart cart={cart} ecom={ecom} onDelCartitem={delCartItem}/>
            </>
        )} />
        <Footer/>
      </Router>
    </div>
  );
}

export default App;
